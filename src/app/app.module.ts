import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeroesService } from './heroes/heroes.service';
import { HeroesComponent } from './heroes/heroes.component';

@NgModule( {
    declarations: [
        AppComponent,
        HeroesComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        HttpClientModule,
        AppRoutingModule
    ],
    providers: [ HeroesService ],
    bootstrap: [ AppComponent ]
} )
export class AppModule { }
