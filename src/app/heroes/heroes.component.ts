import { Component, OnInit } from '@angular/core';

import { HeroesService } from './heroes.service';
import { Hero } from './hero';


@Component( {
    selector: 'app-heroes',
    templateUrl: './heroes.component.html',
    styleUrls: ['./heroes.component.less']
} )
export class HeroesComponent implements OnInit {

    title: string = 'angularapp';
    heroes: Hero[];

    constructor( private heroesService: HeroesService ) { }

    ngOnInit(): void {
        this.getRestItems();
    }

    getRestItems(): void {
        
        this.heroesService.restItemsServiceGetRestItems().subscribe( (heroes: Hero[]) => { this.heroes = heroes; } );

    }
}
