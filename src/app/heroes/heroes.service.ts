import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment'; 

import { Hero } from './hero';


@Injectable( {
    providedIn: 'root'
} )
export class HeroesService {

    heroes: Hero[];
    backend_server_host_ip_and_port: string ;

    constructor( private httpClient: HttpClient ) { }

    restItemsServiceGetRestItems(): Observable<Hero[]> {
        
        this.backend_server_host_ip_and_port = environment.backend_server_host_ip_and_port; 
        
        return this.httpClient.get<Hero[]>( this.backend_server_host_ip_and_port+"/heroes" ).pipe( map( (heroes: Hero[]) => heroes ) );

    }

}